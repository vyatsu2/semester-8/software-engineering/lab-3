﻿#Customer
CREATE TABLE customer
(
    "id"    bigint       NOT NULL,
    name    varchar(100) NOT NULL,
    address varchar(100) NOT NULL,
    phone   varchar(50)  NOT NULL CHECK ( phone ~ '^\+7\d{10}$' ),
    contact varchar(100) NOT NULL,
    CONSTRAINT PK_1 PRIMARY KEY ("id")
);

#Delivery
CREATE TABLE delivery
(
    "id"    bigint       NOT NULL,
    cost    money        NOT NULL CHECK ( cost >= 0 ),
    address varchar(100) NOT NULL,
    type    varchar(50)  NOT NULL,
    CONSTRAINT PK_1 PRIMARY KEY ("id")
);

#Order
CREATE TABLE "order"
(
    "id"        bigint    NOT NULL,
    customer_id bigint    NOT NULL,
    delivery_id bigint    NOT NULL,
    "date"      timestamp NOT NULL,
    CONSTRAINT PK_1 PRIMARY KEY ("id"),
    CONSTRAINT FK_3 FOREIGN KEY (customer_id) REFERENCES customer ("id")
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT FK_5 FOREIGN KEY (delivery_id) REFERENCES delivery ("id")
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE INDEX FK_3 ON "order"
    (
     customer_id
        );

CREATE INDEX FK_3_1 ON "order"
    (
     delivery_id
        );

#Order_product
CREATE TABLE order_product
(
    product_id bigint NOT NULL,
    order_id   bigint NOT NULL,
    "count"    int    NOT NULL CHECK ( count > 0 ),
    CONSTRAINT PK_1 PRIMARY KEY (product_id, order_id),
    CONSTRAINT FK_1 FOREIGN KEY (product_id) REFERENCES product ("id")
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT FK_4_1 FOREIGN KEY (order_id) REFERENCES "order" ("id")
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE INDEX FK_2 ON order_product
    (
     product_id
        );

CREATE INDEX FK_3 ON order_product
    (
     order_id
        );

#Product
CREATE TABLE product
(
    "id"         bigint       NOT NULL,
    product_name varchar(100) NOT NULL,
    price        money        NOT NULL CHECK ( price >= 0 ),
    description  varchar(300) NOT NULL,
    CONSTRAINT PK_1 PRIMARY KEY ("id")
);

#Product_delivery
CREATE TABLE product_delivery
(
    product_id  bigint NOT NULL,
    delivery_id bigint NOT NULL,
    CONSTRAINT PK_1 PRIMARY KEY (product_id, delivery_id),
    CONSTRAINT FK_7 FOREIGN KEY (delivery_id) REFERENCES delivery ("id")
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT FK_6 FOREIGN KEY (product_id) REFERENCES product ("id")
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE INDEX FK_2 ON product_delivery
    (
     delivery_id
        );

CREATE INDEX FK_3 ON product_delivery
    (
     product_id
        );